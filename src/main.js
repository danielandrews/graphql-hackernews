import { GraphQLServer } from 'graphql-yoga'
import { Prisma } from 'prisma-binding'
import path from 'path';

const resolvers = {
    Query: {
        info: () => `This is the API of a Hackernews Clone`,
        feed: (root, args, context, info) => {
            return context.db.query.links({}, info)
        },
        link: (root, args) => {
            return links.find(link => link.id === args.id)
        }
    },
    Mutation: {
        post: (root, args, context, info) => {
            const { url, description } = args;
            return context.db.mutation.createLink({
                data: {
                    url,
                    description
                }
            }, info)
        },
        updateLink: (root, args) => {
            if (!links.some(link => link.id === args.id)) throw Error("Link does not alreay exist")
            links = links.map(link => {
                if (link.id !== args.id) return link
                return { ...args }
            })
            return { ...args };
        },
        deleteLink: (root, args) => {
            const link = links.find(link => link.id === args.id)
            if (!link) throw Error("Link does not alreay exist")
            links = links.filter(link => link.id !== args.id)
            return link;
        },
    },
}

// 3
const server = new GraphQLServer({
    typeDefs: path.join(__dirname, 'schema.graphql'),
    resolvers,
    context: req => ({
        ...req,
        db: new Prisma({
            typeDefs: 'src/generated/prisma.graphql',
            endpoint: 'https://eu1.prisma.sh/dan-8effdf/database/dev',
            secret: 'mysecret123',
            debug: true,
        })
    })
})
server.start(() => console.log(`Server is running on http://localhost:4000`))